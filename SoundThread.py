
import threading
from SoundRecorder import SoundRecorder
import time

soundThreadExitFlag = False


class SoundThread(threading.Thread):
    def __init__(self, thread_id, name, stop_event):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.sound_recorder = SoundRecorder()
        self.stop_event = stop_event

    def run(self):
        print "Starting", self.name
        read_operation(self.sound_recorder, self.stop_event)
        print "Exiting", self.name
        self.sound_recorder.stop_recording()
        self.sound_recorder.save_buffer_to_file()

    def read_sound_from_buffer(self):
        return self.sound_recorder.get_sound_buffer()


def read_operation(sound_recorder, stop_event):
    sound_recorder.open_stream()
    while not stop_event.is_set():
        sound_recorder.read_microphone()
    print "Exiting read_operation"

if __name__ == "__main__":
    recording_stop_event = threading.Event()
    thread = SoundThread(1, "Soundthread", recording_stop_event)
    thread.start()

    count = 0
    while count < 10:
        time.sleep(1)
        print count
        count += 1
        print "%s" % (time.ctime(time.time()))
    recording_stop_event.set()
