import pyaudio
import wave
from RingBuffer import RingBuffer


class SoundRecorder():
    def __init__(self,
                 dformat = pyaudio.paInt32,
                 channels = 2,
                 rate = 44100,
                 chunk = 1024,
                 seconds_in_buffer = 5 ):
        self.rate = rate
        self.chunk = chunk
        self.channels = channels
        self.dformat = dformat
        self.seconds_in_buffer = seconds_in_buffer
        buffer_size = int(self.rate / self.chunk * self.seconds_in_buffer )
        print(buffer_size)
        self.buffer = RingBuffer( buffer_size )
        self.output = "file.wav"
        self.audio = pyaudio.PyAudio()
        self.stream = None

    def open_stream(self):
        self.stream = self.audio.open(format=self.dformat, channels=self.channels,
                                      rate=self.rate, input=True, frames_per_buffer=self.chunk)

    def start_limited_recording(self):
        print "recording..."
        for i in range( 0, 1*int(self.rate / self.chunk * self.seconds_in_buffer )):
            data = self.stream.read(self.chunk)
            self.buffer.append(data)

    def read_microphone(self):
        data = self.stream.read(self.chunk)
        self.buffer.append(data)

    def start_recording(self):
        print "recording..."
        self.stream = self.audio.open(format = self.dformat, channels = self.channels,
                            rate = self.rate, input = True, frames_per_buffer = self.chunk)
        while True:
            data = self.stream.read(self.chunk)
            self.buffer.append(data)

    def stop_recording(self):
        self.stream.stop_stream()
        self.stream.close()
        self.audio.terminate()

    def save_buffer_to_file(self):
        waveFile = wave.open(self.output, 'wb')
        waveFile.setnchannels(self.channels)
        waveFile.setsampwidth(self.audio.get_sample_size(self.dformat))
        waveFile.setframerate(self.rate)
        waveFile.writeframes(b''.join(self.buffer))
        waveFile.close()

    def get_sound_buffer(self):
        return self.buffer


if __name__ == "__main__":
    recorder = SoundRecorder()
    recorder.open_stream()
    recorder.start_limited_recording()
    recorder.stop_recording()
    recorder.save_buffer_to_file()
