
from collections import deque


class RingBuffer(deque):
    def __init__(self, size = 0):
        super(RingBuffer, self).__init__(maxlen=size)

if __name__ == "__main__":
    ringbuffer = RingBuffer(15)
    for i in range(30):
        ringbuffer.append(i)
        print ringbuffer
