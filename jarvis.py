from SoundThread import SoundThread, soundThreadExitFlag, threading
import time
import matplotlib.pyplot as plt
from StopThread import StopThread
import numpy as np
recording_stop_event = threading.Event()

recording_thread = SoundThread(1, "recording_thread", recording_stop_event)
recording_thread.start()

stop_thread = StopThread(1, "stop_thread")
stop_thread.start()

count = 0
plt.figure(1)
plt.title('Signal Wave...')
plt.xlabel("Sample")
plt.ylabel("Amplitude")

time.sleep(5)

stopping_signal = False
while stop_thread.isAlive():
    signal = recording_thread.read_sound_from_buffer()
    amplitude = np.fromstring(str(signal), np.int32)
    plt.plot(amplitude)

    print(signal)
    #plt.plot(signal)
    #plt.show()
recording_stop_event.set()



